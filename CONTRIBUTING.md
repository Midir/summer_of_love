# How to contribute

## Expanding the list of companies

You are welcome to add companies to the list that have proven to support the BLM movement, so others can avoid using their products and services.

Only archived links are accepted (use [archive.today](https://archive.today) for this purpose).

The list is in companies.json and each object is listed in the following format:

```
{
    "name": "Company/product name",
    "proof": [
      "https://archived.link1",
      "https://archived.link2",
      ...
    ],
    "url": "https://companyorproduct.com/"
},
```

- name: The company's/product's name
- proof: An array including one or more archived links with proof of their support
- url: The url to the main website

## Functionality

Further contributions to the browser extension could include:

- Extending the functionality to use multiple urls/domains in one object, so parent companies/subsidiaries can be included
- Change the extension icon when a url/company is not in the list
- Include further proof of the destruction the riots caused in the popup description (archived of course!)
- Styling the popup with CSS
