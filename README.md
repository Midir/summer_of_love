# Summer of Love™

This browser extension was built to be a helpful tool to identify companies and/or products that supported the BLM and the resulting riots in summer of 2020.

The list of companies is based on the [Traitors of America List](https://archive.is/VUS4I) which for this purpose was scraped and converted into a JSON list.
While scraping I was able to identify & correct some errors, but can not guarantee to have found every one of them.

If you find errors like wrong links during the use of this extensions, feel free to file an issue or even a PR.

If you want to contribute and/or find additional companies/products that should be added to the list of traitors, take a look at the [contributing guidelines](CONTRIBUTING.md).

_The name of the extensions refers to the democrat Mayor of Seattle, when she let a bunch of first world communists take over parts of the city._
