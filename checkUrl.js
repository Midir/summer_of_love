chrome.tabs.query({ active: true, lastFocusedWindow: true }, (tabs) => {
  // import companies json list
  const data = fetch('./companies.json')
    .then((response) => response.json())
    .then((data) => appendData(data));

  // extract domain from url
  let currentUrl = tabs[0];
  let url = new URL(currentUrl.url);
  let domain = url.hostname;

  // extract main name from domain
  let stops = (domain.match(/\./g) || []).length;
  let company = '';

  if (stops > 1) {
    let divided = domain.split('.');
    company = divided[1];
  } else {
    let divided = domain.split('.');
    company = divided[0];
  }

  // fill the infobox with data
  const appendData = (data) => {
    let mainContainer = document.getElementById('infobox');
    let div = document.createElement('div');
    div.innerHTML = `<h2>Traitors of America</h2> <p>The owner of this domain is not listed in the <a href=https://archive.is/VUS4I>Traitors of America list</a>, but make sure to do your own research on them.</p>`;
    mainContainer.appendChild(div);
    let ul = document.createElement('ul');

    for (let i = 0; i < data.length; i++) {
      if (data[i].url.includes(company)) {
        div.innerHTML = `<h2>Traitors of America</h2>
        <p>The owner of this domain supported BLM, which led to
        <a href=https://web.archive.org/web/20210206060410/https://majorcitieschiefs.com/pdf/news/mcca_report_on_the_2020_protest_and_civil_unrest.pdf>mass riots</a>,
        <a href=https://archive.is/iVPjf>arson and looting</a>, resulted in
        <a href=https://archive.ph/TwE5q>over $1 billion of damage</a>, got
        <a href=https://archive.ph/3Jwlp> lots of people injured</a> and
        <a href=https://archive.ph/l34Ra>even killed</a>.</p>
        <p>Since the whole movement <a href=https://archive.ph/hhoxg>is based on a lie</a> and <a href=https://archive.ph/bUR6P>BLM is a marxist organization</a>, you might want to consider using a different service/product.</p> <h4>Proof:</h4>`;

        div.appendChild(ul);

        for (let j = 0; j < data[i].proof.length; j++) {
          let li = document.createElement('li');
          let a = document.createElement('a');
          a.innerHTML = data[i].proof[j];
          a.href = data[i].proof[j];
          li.appendChild(a);
          ul.appendChild(li);
        }
      }
    }
  };

  // enable new tabs from url clicks
  window.addEventListener('click', (e) => {
    if (e.target.href !== undefined) {
      chrome.tabs.create({ url: e.target.href });
    }
  });
});
